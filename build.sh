#!/bin/bash

# create sql file from character corrected raw GCIDE files
function process {
	echo "....sql start"

	file=CIDE.A-Z
	src=${file}
	dest=${file}.sql
	if [ -s $dest ]
	then
		rm -f $dest
		touch $dest
	fi

	printf "DELETE FROM definitions;\n" > $dest
	printf "INSERT INTO definitions (title, definition) VALUES\n" >> $dest

	# cat the file and pipe through all the changes...
	# trim all line returns
	# replace "\'d8" with nothing
	# replace "</ent> <au>AS</au><br />" with "</ent>"
	# if entry does not have "<br/" after "</ent>" ("</ent><hw>")
	# if entry has "<ent>...<ent>" without closing tag make sure it's closed
	# find/replace <br/ tags
	# if entry has more than one head word, remove "</ent><br /><ent>" tags and put them together
	# find/replace beginning of each entry '<p><ent>' with 'START_OF_ENTRY<p><ent>' marker so we can 'awk FS' on that
	# separate entry title into separate SQL column
	# then remove as much of the "<pr>" tags as possible
	# if length is over 4 OR does not begin with "<--"
	# add END_OF_FILE so we can remove final ",\n" and replace with ";" to make SQL command correct

	cat $src | tr -d '\r\n' | sed "s/\\\'d8//g;s/<\/ent> <au>AS<\/au>/<\/ent>/g;s/<\/ent><hw>/<\/ent><br\/<hw>/g;s/<ent><br/<\/ent><br/g;s/<br\//<br \/>/g;s/<\/ent><br \/><ent>/, /g;s/<mhw>{/<mhw>/g;s/}<\/mhw>/<\/mhw>/g;s/\(<p><ent>\)/START_OF_ENTRY\n\1/g;s/\(<!-- This\)/START_OF_ENTRY\n\1/g" | sed 's/"/\\"/g' | awk 'BEGIN { FS="START_OF_ENTRY" } {
			if($1 != "" && $1 !~ /^<!--/){
				printf("($$%s$$),\n",$1);
			}
		} END {
			printf("END_OF_FILE");
		}' | sed 's/<p><ent>\(.*\)<\/ent><br \/>/\1\$\$,\$\$<p>/g' | sed ':a;N;$!ba;s/,\nEND_OF_FILE/;\n/g' >> $dest

	echo "....sql finish"
}

# do batch find/replace on all CIDE files
function process_sed {
	echo ".... sed start"

	# clear all sed processed files
	file=CIDE.A-Z
	dest=${file}

	# find and replace tags using 'lookup' file
	while read -r line; do
		oldw=${line%% *};
		neww=${line##* };
		sed -i "s/$oldw/$neww/g" ${dest}
	done < lookup.txt

	echo ".... sed finish"

	# explicitly remove any non-utf8 characters
	# having issues with weird apostrophe (see Black Friday "market's")
	# iconv -t utf-8 -c CIDE.A-Z > CIDE.A-Z

	process
}


# first check to see if gcide repo has been cloned
if [ -s gcide ]
then

	echo "CIDE to SQL build process started"

	src=gcide/CIDE.*
	dest=sed
	\cp ${src} ${dest}

	# remove junk from CIDE.U file
	sed -i '982,5228d' sed/CIDE.U

	# put all files into one
	# cat sed/CIDE.X sed/CIDE.Y sed/CIDE.Z > CIDE.A-Z
	# cat sed/CIDE.U > CIDE.A-Z
	# cat sed/CIDE.C > CIDE.A-Z
	cat sed/CIDE.* > CIDE.A-Z
	echo "CIDE parse start"
	process_sed
	echo ".... parse finish"

	echo "CIDE to SQL build process finished"

else

	echo "Please clone the GCIDE git repo before building."
	echo "Run this code to continue:"
	echo "git clone git://git.savannah.gnu.org/gcide.git"

fi


