#!/bin/bash

# get defintions and create pronunciations

echo "espeak started"

dest=espeak.sql
if [ -s $dest ]
then
	rm -f $dest
	touch $dest
fi

# echo "BEGIN;" >> $dest

set -e
set -u

PSQL=/usr/bin/psql

DB_USER=postgres
DB_HOST=localhost
DB_NAME=avuncular

$PSQL \
	-X \
	-h $DB_HOST \
	-U $DB_USER \
	-c 'SELECT id, title FROM "definitions" ORDER BY "id"' \
	--single-transaction \
	--set AUTOCOMMIT=off \
	--set ON_ERROR_STOP=on \
	--no-align \
	-t \
	--field-separator ' ' \
	--quiet \
	-d $DB_NAME \
| while read id title ; do
	if [ title ]
	then
		p=$(espeak -q --ipa ["$title"])
		t=$(echo "$p" | sed 's/^ *//')
		# echo "$id $title $t"
		echo "UPDATE definitions SET pronunciation=\$\$$t\$\$ WHERE id=$id;" >> $dest
	fi
done

# echo "COMMIT;" >> $dest

echo "espeak finished"
